from mysound import Sound


def soundadd(s1: Sound, s2: Sound) -> Sound:
    # Obtener longitudes de buffers
    len1 = len(s1.buffer)
    len2 = len(s2.buffer)

    # Determinar longitud de sonido de salida
    len_out = max(len1, len2)

    # Inicializar buffer de salida
    out_buffer = [0] * len_out

    # Sumar muestras correspondientes
    for i in range(min(len1, len2)):
        out_buffer[i] = s1.buffer[i] + s2.buffer[i]

        # Copiar muestras restantes del sonido más largo
    if len1 > len2:
        out_buffer[len2:] = s1.buffer[len2:]
    else:
        out_buffer[len1:] = s2.buffer[len1:]

    # Crear y devolver objeto Sound de salida
    out_sound = Sound(max(s1.duration, s2.duration))
    out_sound.buffer = out_buffer
    return out_sound
