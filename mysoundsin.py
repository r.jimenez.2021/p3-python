from mysound import Sound


class SoundSin(Sound):
    def __init__(self, duracion, frecuencia, amplitud):
        super().__init__(duracion)  # herenciamos el init de Sound

        self.frecuencia = frecuencia
        self.amplitud = amplitud

        self.sin(frecuencia, amplitud)
        # creamos la señal senoidal en el constructor

    def get_duracion(self):
        return self.duration
