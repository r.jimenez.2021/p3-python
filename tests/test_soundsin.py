import unittest
from mysoundsin import SoundSin


class TestSoundsin(unittest.TestCase):
    def testCreaSenal(self):
        sonido = SoundSin(1, 1000, 500)
        self.assertEqual(len(sonido.buffer), 44100)

    def testGetDuracion(self):
        sonido = SoundSin(1, 1000, 500)
        self.assertEqual(sonido.get_duracion(), 1)

    def testValorSenal(self):
        freq = 1000
        sonido = SoundSin(1, freq, 500)
        periodo = sonido.samples_second / freq
        self.assertAlmostEqual(-500, sonido.buffer[int(periodo * 0.75)], delta=1)


if __name__ == '__main__':
    unittest.main()
