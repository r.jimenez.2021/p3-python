import unittest
from mysoundsin import SoundSin
from soundops import soundadd


class TestSoundAdd(unittest.TestCase):

    def test_queduracion(self):  # Comprueba que la duracion de la suma es la maxima entre ambas
        s1 = SoundSin(1, 440, 100)
        s2 = SoundSin(4, 880, 200)
        resultado = soundadd(s1, s2)
        esperado = max(s1.duration, s2.duration)
        self.assertEqual(resultado.duration, esperado)

    def test_queamplitud(self):  # Al tener misma frecuencia la amplitud maxima es la suma de ambas
        s1 = SoundSin(1, 440, 100)
        s2 = SoundSin(4, 440, 200)
        resultado = soundadd(s1, s2)
        esperado = 300
        periodo = resultado.samples_second / 440
        self.assertAlmostEqual(resultado.buffer[int(periodo * 0.25)], esperado, delta=2)

    def test_sonidos_vacios(self):  # Comprueba que si uno dura mas,
        # apartir de que se acabe el corto solo queda el buffer del largo
        s1 = SoundSin(1, 440, 100)
        s2 = SoundSin(4, 440, 200)
        resultado = soundadd(s1, s2)
        esperado = s2.buffer[len(s1.buffer):]
        self.assertEqual(resultado.buffer[len(s1.buffer):], esperado)


if __name__ == '__main__':
    unittest.main()
