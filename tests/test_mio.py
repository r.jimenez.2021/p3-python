# Ejercicio 4
import unittest
from mysound import Sound


class TestSound(unittest.TestCase):

    def test_sine_wave(self):
        freq = 1000
        amp = 500
        sound = Sound(1)
        sound.sin(freq, amp)
        periodo = sound.samples_second / freq  # periodo de la onda senoidal
        # Comprobar amplitud en muestra 0 (cualquier valor por el periodo deberia salir 0)
        self.assertEqual(0, sound.buffer[int(0 * periodo)])
        self.assertAlmostEqual(0, sound.buffer[int(periodo)],
                               delta=8)  # deberia salir pero ponemos los deltas por los decimales que no coge
        self.assertAlmostEqual(500, sound.buffer[int(periodo * 0.25)], delta=1)
        self.assertAlmostEqual(0, sound.buffer[int(periodo * 0.5)], delta=4)
        self.assertAlmostEqual(500, sound.buffer[int(periodo * 1.25)], delta=1)


if __name__ == '__main__':
    unittest.main()
